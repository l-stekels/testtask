# Test task
Test task for PHP Developer position.

## Structure
```
ci/
src/
tests/
vendor/
```

## Install
To install dependencies:

``` bash
$ composer install
```

## Testing
``` bash
$ composer test
```

## PHP Code Sniffer
``` bash
$ composer phpcs
```