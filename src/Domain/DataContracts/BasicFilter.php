<?php

namespace TestTask\Domain\DataContracts;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use TestTask\Domain\ValueObjects\Product;

class BasicFilter
{
    /**
     * @var DateTimeImmutable|false
     */
    public DateTimeImmutable $from;

    /**
     * @var DateTimeImmutable|false
     */
    public DateTimeImmutable $to;

    /**
     * @var DateTimeImmutable|false
     */
    public DateTimeImmutable $previousFrom;

    /**
     * @var DateTimeImmutable|false
     */
    public DateTimeImmutable $previousTo;

    /**
     * @var array|Product[]
     */
    public array $products;

    /**
     * BasicFilter constructor.
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param Product[] $products
     * @throws Exception
     */
    public function __construct(DateTimeInterface $from, DateTimeInterface $to, array $products)
    {
        $this->from = DateTimeImmutable::createFromFormat(DATE_ISO8601, $from->format(DATE_ISO8601));
        $this->to = DateTimeImmutable::createFromFormat(DATE_ISO8601, $to->format(DATE_ISO8601));
        $this->products = $products;
        $this->calculatePreviousDates();
    }

    /**
     * @throws Exception
     */
    protected function calculatePreviousDates()
    {
        $startOfYear = $this->from->modify('first day of January');
        $endOfYear = $this->to->modify('last day of December');
        if ($this->from->diff($startOfYear)->days === 0 && $this->to->diff($endOfYear)->days === 0) {
            $this->previousFrom = $this->from->modify('first day of January last year');
            $this->previousTo = $this->to->modify('last day of December last year')->setTime(23, 59, 59);
            return;
        }

        $startOfMonth = $this->from->modify('first day of');
        $endOfMonth = $this->from->modify('last day of');
        if ($this->from->diff($startOfMonth)->days === 0 && $this->to->diff($endOfMonth)->days === 0) {
            $this->previousFrom = $this->from->modify('first day of previous month');
            $this->previousTo = $this->to->modify('last day of previous month')->setTime(23, 59, 59);
            return;
        }

        // Add one day because the time 23:59:59 is not counted as a full day.
        $diffInDays = $this->from->diff($this->to)->days + 1;
        $this->previousFrom = $this->from->modify("- {$diffInDays} days");
        $this->previousTo = $this->to->modify("- {$diffInDays} days");
    }
}
