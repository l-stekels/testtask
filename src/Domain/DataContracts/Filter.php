<?php

namespace TestTask\Domain\DataContracts;

class Filter extends BasicFilter
{
    /**
     * @var string[]
     */
    public array $merchants;
}
