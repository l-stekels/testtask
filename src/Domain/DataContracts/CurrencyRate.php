<?php

namespace TestTask\Domain\DataContracts;

use TestTask\Domain\ValueObjects\Currency;

class CurrencyRate
{
    /**
     * @var Currency
     */
    public Currency $targetCurrency;

    /**
     * @var string
     */
    public string $baseCurrency;

    /**
     * @var string
     */
    public string $exchangeRate;
}
