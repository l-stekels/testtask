<?php

namespace TestTask\Domain\ValueObjects;

use TestTask\Exceptions\ValueObject\InvalidCurrencyValue;

class Currency implements ValueObjectInterface
{
    public const EUR = 'EUR';

    public const GBP = 'GBP';

    public const USD = 'USD';

    /**
     * @var string
     */
    protected string $value;

    /**
     * Currency constructor.
     * @param string $value
     * @throws InvalidCurrencyValue
     */
    public function __construct(string $value)
    {
        if (!in_array($value, self::getValidValues())) {
            throw new InvalidCurrencyValue($value);
        }
        $this->value = $value;
    }

    /** @inheritDoc */
    public static function getValidValues(): array
    {
        return [
            self::EUR,
            self::GBP,
            self::USD,
        ];
    }

    /** @inheritDoc */
    public function getValue(): string
    {
        return $this->value;
    }

    /** @inheritDoc */
    public function equals(ValueObjectInterface $valueObject): bool
    {
        return $this->value === $valueObject->getValue();
    }
}
