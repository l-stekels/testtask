<?php

namespace TestTask\Domain\ValueObjects;

use TestTask\Exceptions\ValueObject\InvalidProductName;

class Product implements ValueObjectInterface
{
    public const ECOM = 'ECOM';

    public const POS = 'POS';

    /**
     * @var string
     */
    protected string $value;

    /**
     * Product constructor.
     * @param string $value
     * @throws InvalidProductName
     */
    public function __construct(string $value)
    {
        if (!in_array($value, self::getValidValues())) {
            throw new InvalidProductName($value);
        }
        $this->value = $value;
    }

    /** @inheritDoc */
    public static function getValidValues(): array
    {
        return [
            self::ECOM,
            self::POS,
        ];
    }

    /** @inheritDoc */
    public function getValue(): string
    {
        return $this->value;
    }

    /** @inheritDoc */
    public function equals(ValueObjectInterface $valueObject): bool
    {
        return $this->value === $valueObject->getValue();
    }
}
