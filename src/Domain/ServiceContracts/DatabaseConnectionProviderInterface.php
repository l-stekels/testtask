<?php

namespace TestTask\Domain\ServiceContracts;

use Aura\Sql\ExtendedPdo;

interface DatabaseConnectionProviderInterface
{
    /**
     * @return ExtendedPdo
     */
    public function getConnection(): ExtendedPdo;
}
