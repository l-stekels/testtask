<?php

namespace TestTask\Exceptions;

use RuntimeException;
use Throwable;

class MissingQueryParameterException extends RuntimeException
{
    /**
     * @var string
     */
    protected string $missingParam;

    /**
     * MissingQueryParameterException constructor.
     * @param string $missingParam
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $missingParam, $code = 0, Throwable $previous = null)
    {
        $this->missingParam = $missingParam;
        parent::__construct(
            "Parameter {$missingParam} is missing in the query",
            $code,
            $previous
        );
    }

    /**
     * @return string
     */
    public function getMissingParam(): string
    {
        return $this->missingParam;
    }
}
