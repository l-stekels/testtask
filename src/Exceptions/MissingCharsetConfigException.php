<?php

namespace TestTask\Exceptions;

use LogicException;
use Throwable;

class MissingCharsetConfigException extends LogicException
{
    /**
     * MissingCharsetConfigException constructor.
     * @param string $dsn
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $dsn, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            "Parameter \"charset\" is missing in the dsn: \"{$dsn}\"",
            $code,
            $previous
        );
    }
}
