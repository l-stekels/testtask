<?php

namespace TestTask\Exceptions\ValueObject;

use Exception;
use ReflectionClass;
use TestTask\Domain\ValueObjects\ValueObjectInterface;
use Throwable;

abstract class AbstractInvalidValueException extends Exception
{
    /**
     * AbstractInvalidValueException constructor.
     * @param string $value
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $value, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            "{$value} is not a valid value for {$this->shortClassName()}. Valid values: {$this->validValues()}",
            $code,
            $previous
        );
    }

    /**
     * @return ValueObjectInterface
     */
    abstract protected function getValueObjectClass(): ValueObjectInterface;

    /**
     * @return string
     */
    private function validValues(): string
    {
        return implode(', ', $this->getValueObjectClass()::getValidValues());
    }

    /**
     * @return string
     */
    private function shortClassName(): string
    {
        return (new ReflectionClass($this->getValueObjectClass()))->getShortName();
    }
}
