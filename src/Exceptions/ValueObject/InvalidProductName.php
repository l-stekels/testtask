<?php

namespace TestTask\Exceptions\ValueObject;

use TestTask\Domain\ValueObjects\Product;
use TestTask\Domain\ValueObjects\ValueObjectInterface;

class InvalidProductName extends AbstractInvalidValueException
{
    /**
     * @return ValueObjectInterface
     */
    protected function getValueObjectClass(): ValueObjectInterface
    {
        return new Product('');
    }
}
