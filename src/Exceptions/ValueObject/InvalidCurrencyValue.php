<?php

namespace TestTask\Exceptions\ValueObject;

use TestTask\Domain\ValueObjects\Currency;
use TestTask\Domain\ValueObjects\ValueObjectInterface;

class InvalidCurrencyValue extends AbstractInvalidValueException
{
    /**
     * @return ValueObjectInterface
     */
    protected function getValueObjectClass(): ValueObjectInterface
    {
        return new Currency('');
    }
}
