<?php

namespace TestTask\Utils;

use Aura\Sql\ExtendedPdo;
use TestTask\Domain\ServiceContracts\DatabaseConnectionProviderInterface;
use TestTask\Exceptions\MissingCharsetConfigException;

class DatabaseConnectionProvider implements DatabaseConnectionProviderInterface
{
    /**
     * @var ExtendedPdo
     */
    protected ExtendedPdo $pdo;

    /**
     * DatabaseConnectionProvider constructor.
     * @param string $pdoDsn
     * @param string $username
     * @param string $password
     * @throws MissingCharsetConfigException
     */
    public function __construct(string $pdoDsn, string $username, string $password)
    {
        if (!preg_match("/charset=.+/", $pdoDsn)) {
            throw new MissingCharsetConfigException($pdoDsn);
        }
        $this->pdo = new ExtendedPdo($pdoDsn, $username, $password);
    }

    /** @inheritDoc */
    public function getConnection(): ExtendedPdo
    {
        return $this->pdo;
    }
}
