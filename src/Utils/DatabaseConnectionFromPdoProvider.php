<?php

namespace TestTask\Utils;

use Aura\Sql\DecoratedPdo;
use Aura\Sql\ExtendedPdo;
use PDO;
use TestTask\Domain\ServiceContracts\DatabaseConnectionProviderInterface;

class DatabaseConnectionFromPdoProvider implements DatabaseConnectionProviderInterface
{
    /**
     * @var DecoratedPdo|ExtendedPdo|PDO
     */
    protected ExtendedPdo $pdo;

    public function __construct(PDO $pdo)
    {
        if (! ($pdo instanceof ExtendedPdo)) {
            $pdo = new DecoratedPdo($pdo);
        }
        $this->pdo = $pdo;
    }

    public function getConnection(): ExtendedPdo
    {
        return $this->pdo;
    }
}
