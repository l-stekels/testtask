<?php

namespace TestTask\Utils;

use TestTask\Domain\ValueObjects\Currency;
use TestTask\Domain\ValueObjects\Product;
use TestTask\Exceptions\MissingQueryParameterException;
use TestTask\Exceptions\ValueObject\InvalidProductName;

class Util
{
    /**
     * @param array $params
     * @return Currency
     * @throws MissingQueryParameterException
     */
    public static function baseCurrencyFromRequestQuery(array $params): Currency
    {
        if (!isset($params['base_currency'])) {
            throw new MissingQueryParameterException('base_currency');
        }
        return new Currency($params['base_currency']);
    }

    /**
     * @param array $params
     * @return Product[]
     * @throws InvalidProductName
     */
    public static function productsFromRequestQuery(array $params): array
    {
        $productsList = $params['products'] ?? [];

        if (!is_array($productsList)) {
            return [new Product($productsList)];
        }

        foreach ($productsList as $value) {
            $products[] = new Product($value);
        }

        return $products ?? [];
    }
}
