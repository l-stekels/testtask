<?php

namespace TestTask\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use TestTask\Domain\DataContracts\BasicFilter;

class BasicFilterTest extends TestCase
{
    /**
     * @dataProvider firstAndLastDaysOfAYear
     * @test
     * @param $expected
     * @param $given
     * @throws Exception
     */
    public function it_correctly_calculates_previous_from_and_to_for_first_and_last_days_of_a_year($expected, $given)
    {
        $basicFilter = new BasicFilter(new \DateTime($given['from']), new \DateTime($given['to']), []);

        $this->assertEquals(new \DateTimeImmutable($expected['from']), $basicFilter->previousFrom);
        $this->assertEquals(new \DateTimeImmutable($expected['to']), $basicFilter->previousTo);
    }

    /**
     * @dataProvider firstAndLastDaysOfAMonth
     * @test
     * @param $expected
     * @param $given
     * @throws Exception
     */
    public function it_correctly_calculates_previous_from_and_to_for_first_and_last_days_of_month($expected, $given)
    {
        $basicFilter = new BasicFilter(new \DateTime($given['from']), new \DateTime($given['to']), []);

        $this->assertEquals(new \DateTimeImmutable($expected['from']), $basicFilter->previousFrom);
        $this->assertEquals(new \DateTimeImmutable($expected['to']), $basicFilter->previousTo);
    }

    /**
     * @dataProvider dateIntervals
     * @test
     */
    public function it_correctly_calculates_previous_from_and_to_for_date_interval($expected, $given)
    {
        $basicFilter = new BasicFilter(new \DateTime($given['from']), new \DateTime($given['to']), []);

        $this->assertEquals(new \DateTimeImmutable($expected['from']), $basicFilter->previousFrom);
        $this->assertEquals(new \DateTimeImmutable($expected['to']), $basicFilter->previousTo);
    }

    public function firstAndLastDaysOfAYear()
    {
        return [
            [
                'expected' => ['from' => '2019-01-01 00:00:00', 'to' => '2019-12-31 23:59:59'],
                'given' => ['from' => '2020-01-01 00:00:00', 'to' => '2020-12-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2018-01-01 00:00:00', 'to' => '2018-12-31 23:59:59'],
                'given' => ['from' => '2019-01-01 00:00:00', 'to' => '2019-12-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2017-01-01 00:00:00', 'to' => '2017-12-31 23:59:59'],
                'given' => ['from' => '2018-01-01 00:00:00', 'to' => '2018-12-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2016-01-01 00:00:00', 'to' => '2016-12-31 23:59:59'],
                'given' => ['from' => '2017-01-01 00:00:00', 'to' => '2017-12-31 23:59:59'],
            ],
        ];
    }

    public function firstAndLastDaysOfAMonth()
    {
        return [
            [
                'expected' => ['from' => '2019-12-01 00:00:00', 'to' => '2019-12-31 23:59:59'],
                'given' => ['from' => '2020-01-01 00:00:00', 'to' => '2020-01-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2020-01-01 00:00:00', 'to' => '2020-01-31 23:59:59'],
                'given' => ['from' => '2020-02-01 00:00:00', 'to' => '2020-02-29 23:59:59'],
            ],
            [
                'expected' => ['from' => '2019-11-01 00:00:00', 'to' => '2019-11-30 23:59:59'],
                'given' => ['from' => '2019-12-01 00:00:00', 'to' => '2019-12-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2019-11-01 00:00:00', 'to' => '2019-11-30 23:59:59'],
                'given' => ['from' => '2019-12-01 00:00:00', 'to' => '2019-12-31 23:59:59'],
            ],
            [
                'expected' => ['from' => '2019-04-01 00:00:00', 'to' => '2019-04-30 23:59:59'],
                'given' => ['from' => '2019-05-01 00:00:00', 'to' => '2019-05-31 23:59:59'],
            ],
        ];
    }

    public function dateIntervals()
    {
        return [
            'one day' => [
                'expected' => ['from' => '2020-05-17 00:00:00', 'to' => '2020-05-17 23:59:59'],
                'given' => ['from' => '2020-05-18 00:00:00', 'to' => '2020-05-18 23:59:59'],
            ],
            'four days' => [
                'expected' => ['from' => '2020-05-14 00:00:00', 'to' => '2020-05-17 23:59:59'],
                'given' => ['from' => '2020-05-18 00:00:00', 'to' => '2020-05-21 23:59:59'],
            ],
            'fifteen days' => [
                'expected' => ['from' => '2020-04-17 00:00:00', 'to' => '2020-05-02 23:59:59'],
                'given' => ['from' => '2020-05-03 00:00:00', 'to' => '2020-05-18 23:59:59'],
            ],
            '35 days' => [
                'expected' => ['from' => '2020-03-28 00:00:00', 'to' => '2020-05-02 23:59:59'],
                'given' => ['from' => '2020-05-03 00:00:00', 'to' => '2020-06-07 23:59:59'],
            ],
            '1000 days' => [
                'expected' => ['from' => '2017-04-28 00:00:00', 'to' => '2020-05-02 23:59:59'],
                'given' => ['from' => '2020-05-03 00:00:00', 'to' => '2023-05-08 23:59:59'],
            ],
        ];
    }
}
