<?php

namespace TestTask\Tests;

use Aura\Sql\ExtendedPdo;
use PHPUnit\Framework\TestCase;
use TestTask\Exceptions\MissingCharsetConfigException;
use TestTask\Utils\DatabaseConnectionProvider;

class DatabaseConnectionProviderTest extends TestCase
{
    /** @test */
    public function it_returns_an_instance_of_extended_pdo()
    {
        $provider = new DatabaseConnectionProvider(
            'mysql:host=localhost;dbname=testdb;charset=utf8',
            'username',
            'password'
        );

        $this->assertInstanceOf(ExtendedPdo::class, $provider->getConnection());
    }

    /** @test */
    public function it_throws_missing_charset_config_exception_if_charset_is_missing_from_dsn()
    {
        $this->expectException(MissingCharsetConfigException::class);
        new DatabaseConnectionProvider('mysql:host=localhost;dbname=testdb', 'username', 'password');
    }
}