<?php

namespace TestTask\Tests;

use PHPUnit\Framework\TestCase;
use TestTask\Domain\ValueObjects\Currency;
use TestTask\Domain\ValueObjects\Product;
use TestTask\Exceptions\MissingQueryParameterException;
use TestTask\Utils\Util;

class UtilTest extends TestCase
{
    /** @test */
    public function it_returns_currency_object_when_base_currency_from_request_query_is_called()
    {
        $result = Util::baseCurrencyFromRequestQuery(['base_currency' => 'EUR']);
        $this->assertInstanceOf(Currency::class, $result);
        $this->assertEquals(Currency::EUR, $result->getValue());
    }

    /** @test */
    public function it_throws_exception_if_base_currency_from_request_query_is_called_without_base_currency_param()
    {
        $this->expectException(MissingQueryParameterException::class);
        Util::baseCurrencyFromRequestQuery([]);
    }

    /** @test */
    public function it_returns_an_array_of_product_objects_from_products_from_request_query()
    {
        $result = Util::productsFromRequestQuery(['products' => ['ECOM', 'POS']]);
        $this->assertCount(2, $result);
        $this->assertInstanceOf(Product::class, $result[0]);
        $this->assertInstanceOf(Product::class, $result[1]);
        $this->assertEquals(Product::ECOM, $result[0]->getValue());
        $this->assertEquals(Product::POS, $result[1]->getValue());
    }
}